import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup } from '@angular/forms';
import { evaluate } from 'mathjs';

@Component({
  selector: 'app-jacobi',
  templateUrl: './jacobi.component.html',
  styleUrls: ['./jacobi.component.css']
})
export class JacobiComponent implements OnInit {

  jacobiform:FormGroup;
  //jacobi!:jacobiModel[]
  result:number;

  constructor(private form:FormBuilder) {
    this.jacobiform = this.form.group({

    })
    this.result = 0;
   }

  ngOnInit(): void {
  }

}
