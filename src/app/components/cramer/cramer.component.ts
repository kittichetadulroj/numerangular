import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { evaluate } from 'mathjs';
@Component({
  selector: 'app-cramer',
  templateUrl: './cramer.component.html',
  styleUrls: ['./cramer.component.css']
})
export class CramerComponent implements OnInit {

  cramerform:FormGroup
  //cramer!:cramerModel[]
  result:number;

  constructor(private form:FormBuilder) {
    this.cramerform = this.form.group({

    })
    this.result = 0;
   }

  ngOnInit(): void {
  }

}
