import { Component, OnInit } from '@angular/core';
import { Newtonclass } from './newtonclass.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { derivative, evaluate } from 'mathjs';

@Component({
  selector: 'app-newton',
  templateUrl: './newton.component.html',
  styleUrls: ['./newton.component.css']
})
export class NewtonComponent implements OnInit {
  newtonform:FormGroup;
  newton !:Newtonclass[];
  result:number;
  fx:string;
  x0:number;
  labels:string[];

  constructor(private form:FormBuilder) {
    this.newtonform = this.form.group({
      FX:'',
      X0:'0.0'
    })
    this.fx="";
    this.x0=0;
    this.result=0;
    this.labels = [];
   }

  ngOnInit(): void {
  }

  run(newtondata:Newtonclass){
    //save to show fx,x0
    this.fx=this.newtonform.get("FX")?.value;
    this.x0=this.newtonform.get('X0')?.value;
    //function take equation
    function take(x:number,equation:string):number{
      console.log("its working take")
      let num = String(x);
      return evaluate(equation.replace(/x/gi,num))
    }
    function takeprime(x:number,equation:string){
      console.log("its working takePrime")
      let num = String(x);
      let diff = String(derivative(equation,'x'));
      return evaluate(diff.replace(/x/gi,num))
    }

    //let take = (x:number)=>(x*x)-7;
    //let takeprime = (x:number)=>2*x;
    let x=0.0;
    let abs=0;
    let e = 0.000001;
    //declare count for count iteration
    let count = 1;
    do{
        let FX = take(newtondata.X0,this.fx);
        console.log("This is FX: "+FX);
        let fprimex = takeprime(newtondata.X0,this.fx);
        console.log("This is FX prime: "+fprimex);
        x = newtondata.X0-(FX/fprimex);

        abs = Math.abs((x-newtondata.X0)/x);

        newtondata.X0=x;
        console.log(count);
        count++;
        //calling graph
        this.newDataPoint(x,count.toString())
    }
    while(abs>e){
        this.result = x;
    }

  }
  //graph setting
  chartData = [
    {
      data: [0],
      label: 'x'
    },
  ];
  chartLabels = ['1'];
  chartOptions = {
    responsive: true

  };
  //graph dynamic data
  newDataPoint(data:number, label:string) {
    console.log("its working")
    this.chartData.forEach((dataset, index) => {
      this.chartData[index] = Object.assign({}, this.chartData[index], {
        data: [...this.chartData[index].data, data]
      });
    });
    this.chartLabels = [...this.chartLabels, label];
  }

}
