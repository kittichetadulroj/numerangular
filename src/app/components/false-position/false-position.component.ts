import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { evaluate } from 'mathjs';
import { FalsePositionClass } from './false-position-class.model';

@Component({
  selector: 'app-false-position',
  templateUrl: './false-position.component.html',
  styleUrls: ['./false-position.component.css']
})
export class FalsePositionComponent implements OnInit {
  falsepositionform : FormGroup;
  falsepositon !: FalsePositionClass[];
  result:number;
  fx:string;
  xl:number;
  xr:number;
  labels:string[];


  constructor(private form: FormBuilder ) {
    this.falsepositionform = this.form.group({
      FX:'',
      XL:'0',
      XR:'0'

    })
    this.fx='';
    this.xl=0;
    this.xr=0;
    this.result = 0;
    this.labels = [];
   }

  ngOnInit(): void {
  }

  run(falseposiotiondata:FalsePositionClass){
    //save to show data
    this.fx=this.falsepositionform.get('FX')?.value;
    this.xl=this.falsepositionform.get('XL')?.value;
    this.xr=this.falsepositionform.get('XR')?.value;
    //function take equation
    function f(x:number,equation:string):number{
      let num = String(x);
      return evaluate(equation.replace(/x/gi,num))
    }


    //let f = (x:number)=>(x-(1/43));
    let cal_x1 = (l:number,r:number)=>(((l*f(r,this.fx))-(r*f(l,this.fx)))/(f(r,this.fx)-f(l,this.fx)));
    let Xnew=0,Xold=0,res=0,e=0.000001;
    //declare count for count iteration
    let count = 1;

    do{
      let x1 = cal_x1(falseposiotiondata.XL,falseposiotiondata.XR);
      if(f(x1,this.fx)*f(falseposiotiondata.XR,this.fx)<0){

          falseposiotiondata.XL = x1;
          Xnew = falseposiotiondata.XL;

      }
      else{
        falseposiotiondata.XR = x1;
          Xnew = falseposiotiondata.XR;
      }
      res = Math.abs((Xnew-Xold)/Xnew);
      Xold = Xnew;
      console.log(count);
      count++;
      //calling graph
      this.newDataPoint(Xnew,count.toString())

  }
  while(res>e){
      return this.result=Xnew;
  }
  }


  //graph setting
  chartData = [
    {
      data: [0],
      label: 'Xnew'
    },
  ];
  chartLabels = ['1'];
  chartOptions = {
    responsive: true

  };
  //graph dynamic data
  newDataPoint(data:number, label:string) {
    console.log("its working")
    this.chartData.forEach((dataset, index) => {
      this.chartData[index] = Object.assign({}, this.chartData[index], {
        data: [...this.chartData[index].data, data]
      });
    });
    this.chartLabels = [...this.chartLabels, label];
  }



}
