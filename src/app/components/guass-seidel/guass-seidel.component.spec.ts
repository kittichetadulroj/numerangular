import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuassSeidelComponent } from './guass-seidel.component';

describe('GuassSeidelComponent', () => {
  let component: GuassSeidelComponent;
  let fixture: ComponentFixture<GuassSeidelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuassSeidelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuassSeidelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
