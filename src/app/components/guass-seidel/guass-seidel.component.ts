import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { evaluate } from 'mathjs';

@Component({
  selector: 'app-guass-seidel',
  templateUrl: './guass-seidel.component.html',
  styleUrls: ['./guass-seidel.component.css']
})
export class GuassSeidelComponent implements OnInit {
  guassSeidelform:FormGroup;
  // guassSeidel!:guassSeidelModel[];
  result:number

  constructor(private form:FormBuilder) {
    this.guassSeidelform = this.form.group({

    })
    this.result = 0;
  }

  ngOnInit(): void {
  }

}
