import { Component, OnInit } from '@angular/core';
import { Onepointclass } from './onepointclass.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { evaluate } from 'mathjs';

@Component({
  selector: 'app-onepoint',
  templateUrl: './onepoint.component.html',
  styleUrls: ['./onepoint.component.css']
})
export class OnepointComponent implements OnInit {

  onepointform : FormGroup;
  onepoint !: Onepointclass[];
  result:number;
  fx:string;
  x0:number;
  labels:string[];


  constructor(private form: FormBuilder) {
    this.onepointform = this.form.group({
      FX:'',
      X0:'0.0'
    })
    this.fx='';
    this.x0=0;
    this.result = 0;
    this.labels = [];
   }

  ngOnInit(): void {
  }

  run(onepointdata:Onepointclass){
    //save to show fx and x0 value
    this.fx=this.onepointform.get('FX')?.value;
    this.x0=this.onepointform.get('X0')?.value;
    //function take equation
    function take(x:number,equation:string):number{
      let num = String(x);
      return evaluate(equation.replace(/x/gi,num))
    }

   // let take = (x:number)=>((0.5-x+(10*x))/10);
    let e = 0.000001,res = 0,x=0;
    //declare count for count iteration
    let count = 1;
    do{
        x = take(onepointdata.X0,this.fx);
        res = Math.abs((x-onepointdata.X0)/x);
        onepointdata.X0=x;
        console.log(count);
        count++;
        //calling graph
        this.newDataPoint(x,count.toString())
    }
    while(res>e){
        return this.result=x;
    }
  }
  //graph setting
  chartData = [
    {
      data: [0],
      label: 'XM'
    },
  ];
  chartLabels = ['1'];
  chartOptions = {
    responsive: true

  };
  //graph dynamic data
  newDataPoint(data:number, label:string) {
    console.log("its working")
    this.chartData.forEach((dataset, index) => {
      this.chartData[index] = Object.assign({}, this.chartData[index], {
        data: [...this.chartData[index].data, data]
      });
    });
    this.chartLabels = [...this.chartLabels, label];
  }

}
