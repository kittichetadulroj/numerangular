import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { evaluate } from 'mathjs';
import { bisecModel } from './bisecModel';



@Component({
  selector: 'app-bisection',
  templateUrl: './bisection.component.html',
  styleUrls: ['./bisection.component.css']
})
export class BisectionComponent implements OnInit {

  bisectionform : FormGroup
  bisection !: bisecModel[];
  result:number;
  fx:string;
  xl:number;
  xr:number;
  labels:string[];

  constructor(private form: FormBuilder) {
    this.bisectionform = this.form.group({
      FX:'',
      XL:'0',
      XR:'0'
    })
    this.fx='';
    this.xl = 0;
    this.xr = 0;
    this.result = 0;
    this.labels = [];
  }

  ngOnInit(): void {}

  run(bisectiondata:bisecModel){
        //save to show fx,xl,xr value
        this.fx=this.bisectionform.get('FX')?.value;
        this.xl=this.bisectionform.get('XL')?.value;
        this.xr=this.bisectionform.get('XR')?.value;
        console.log(this.fx)
        console.log(this.xl)
        console.log(this.xr)
        //function take equation
        function take(x:number,equation:string):number{
          let num = String(x);
          return evaluate(equation.replace(/x/gi,num))
        }

        //let take=(x:number)=>(x - Math.pow(13,0.25))
        let mid=(L:number,R:number)=>((L+R)/2.0)
        let cross=(fxM:number,fxR:number)=>(fxM*fxR)
        let abs = (NEW:number,OLD:number)=>(Math.abs((NEW-OLD)/NEW))
        let res = 1,xold=0,XM=0;
        let e = 0.000001;

        //declare count for count iteration
        let count = 1;
        XM=0,xold=0,res=1;
        do{
            XM = mid(bisectiondata.XL,bisectiondata.XR);
            console.log("This is XM = "+XM);

            let fxr = take(bisectiondata.XR,this.fx);
            console.log("This is Fxr = "+fxr);

            let fxm = take(XM,this.fx);
            console.log("This is Fxm = "+fxm);
            if(cross(fxm,fxr)>0){
                xold = bisectiondata.XR;
                bisectiondata.XR = XM;
            }
            else{
                xold = bisectiondata.XL;
                bisectiondata.XL = XM;
            }
            res = abs(XM,xold);
            console.log(count);
            count++;
            //calling graph
            this.newDataPoint(XM,count.toString())

        }
        while(res>e){
            count = 0;
            return this.result=XM;
        }
  }
    //graph setting
    chartData = [
      {
        data: [0],
        label: 'XM'
      },
    ];
    chartLabels = ['1'];
    chartOptions = {
      responsive: true

    };
    //graph dynamic data
    newDataPoint(data:number, label:string) {
      console.log("its working")
      this.chartData.forEach((dataset, index) => {
        this.chartData[index] = Object.assign({}, this.chartData[index], {
          data: [...this.chartData[index].data, data]
        });
      });
      this.chartLabels = [...this.chartLabels, label];
    }

}
