import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { BisectionComponent } from './components/bisection/bisection.component';
import { OnepointComponent } from './components/onepoint/onepoint.component';
import { FalsePositionComponent } from './components/false-position/false-position.component';
import { NewtonComponent } from './components/newton/newton.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgChartsModule } from 'ng2-charts';
import { CramerComponent } from './components/cramer/cramer.component';
import { GuassSeidelComponent } from './components/guass-seidel/guass-seidel.component';
import { JacobiComponent } from './components/jacobi/jacobi.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    BisectionComponent,
    OnepointComponent,
    FalsePositionComponent,
    NewtonComponent,
    CramerComponent,
    GuassSeidelComponent,
    JacobiComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
