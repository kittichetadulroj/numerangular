import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { BisectionComponent } from './components/bisection/bisection.component';
import { FalsePositionComponent } from './components/false-position/false-position.component';
import { OnepointComponent } from './components/onepoint/onepoint.component';
import { NewtonComponent } from './components/newton/newton.component';
import { CramerComponent } from './components/cramer/cramer.component';
import { GuassSeidelComponent } from './components/guass-seidel/guass-seidel.component';
import { JacobiComponent } from './components/jacobi/jacobi.component';


const routes: Routes = [
  {path : "bisection",component:BisectionComponent},
  {path : "falseposiotion", component:FalsePositionComponent},
  {path : "onepoint", component:OnepointComponent},
  {path : "newton", component:NewtonComponent},
  {path : "cramer", component:CramerComponent},
  {path : "guass-seidel", component:GuassSeidelComponent},
  {path : "jacobi", component:JacobiComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
